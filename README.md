## Usage

**1.** Clone this repository locally and enter the cloned folder.

```sh
git clone https://github.com/vanhyr/grub-themes.git
cd grub-themes
```

**2.** Copy the theme folder to `/usr/share/grub/themes/`.

```sh
sudo cp -r {theme_folder} /usr/share/grub/themes/
```

**3.** Uncomment and edit following line in `/etc/default/grub` to selected
theme.

```sh
GRUB_THEME="/usr/share/grub/themes/{theme_folder}/theme.txt"
```

**4.** Update grub

```sh
sudo grub-mkconfig -o /boot/grub/grub.cfg
```
